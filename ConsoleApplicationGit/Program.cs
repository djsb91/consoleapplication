﻿using System;

namespace ConsoleApplicationGit
{
    interface Action
    {
        void Drive();
    }
    public class Car : Action
    {
        public string brand;
        public string model;

        public void Drive()
        {
            Console.WriteLine("Method Drive() - interface Action - class Car");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Car BMW = new Car();
            Car Mercedes = new Car();

            BMW.brand = "BMW";
            Mercedes.brand = "Mercedes";

            Console.WriteLine(BMW.brand);
            BMW.Drive();

            Console.WriteLine(Mercedes.brand);
            Mercedes.Drive();

            Console.ReadKey();

            //Dodałem tekst na master
        }
    }
}
